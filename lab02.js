const _ = require('lodash');

const user ={
    name: 'Imie',
    surname: 'Nazwisko',
    allGrades: [
        {
            subjectName: 'Name1',
            grades: [5,4,3,5,2],
            weight: 3
        },
        {
            subjectName: 'Name2',
            grades: [3,3.5,2],
            weight: 1
        },
        {
            subjectName: 'Name3',
            grades: [4,3,3.5],
            weight: 5
        }
    ]
}

function weightAverage() {
    const { name, surname, allGrades } = user;
    const subject = allGrades.map(value => {
        const { weight, grades } = value;
        const { length } = grades;
        const sum = grades.reduce((acc,elem) => acc+elem);

        return { sum, weight,length};

    });

    const num = subject.map(value => value.sum * value.weight).reduce((acc, element) => acc+element);
    const denum = subject.map(value => value.weight * value.length).reduce((acc, element) => acc+element);

    const avg = num/denum;

    console.log(`Srednia wazona ${name} ${surname} : ${avg}`);
}

weightAverage(user)

//////////////////////////////////////////
// let arr1 = [1,2,3,4];
// console.log(_.mean(arr1));